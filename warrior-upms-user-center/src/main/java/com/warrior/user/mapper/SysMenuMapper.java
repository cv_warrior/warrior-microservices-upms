package com.warrior.user.mapper;

import com.warrior.common.model.SysMenu;
import com.warrior.mysql.mapper.SuperMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单数据库操作
 *
 * @author warrior
 */
@Mapper
public interface SysMenuMapper extends SuperMapper<SysMenu> {
    List<SysMenu> findByUserId(@Param("userId") Long userId, @Param("type") Integer type);
}
