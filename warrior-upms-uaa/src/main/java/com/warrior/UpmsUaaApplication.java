package com.warrior;

import com.warrior.loadbalancer.annotation.EnableFeignInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 用户sso 认证中心服务启动类
 *
 * @author warrior
 */
@EnableFeignClients
@MapperScan(basePackages = "com.warrior.uaa.mapper")
@EnableFeignInterceptor
@EnableDiscoveryClient
@SpringBootApplication
public class UpmsUaaApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(UpmsUaaApplication.class);
        application.setEnvironmentPrefix("warrior-uaa");
        application.run(args);
    }
}
