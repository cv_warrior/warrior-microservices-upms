package com.warrior.uaa.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.warrior.mysql.mapper.SuperMapper;
import com.warrior.uaa.model.Client;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 应用数据库操作接口声明
 *
 * @author warrior
 */
@Mapper
public interface ClientMapper extends SuperMapper<Client> {
    List<Client> findList(Page<Client> page, @Param("params") Map<String, Object> params);
}
