package com.warrior.uaa.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.warrior.common.context.LoginUserContextHolder;
import com.warrior.common.lock.DistributedLock;
import com.warrior.common.model.PageResult;
import com.warrior.common.service.impl.SuperServiceImpl;
import com.warrior.uaa.mapper.ClientMapper;
import com.warrior.uaa.model.Client;
import com.warrior.uaa.service.IClientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 应用操作服务接口实现
 *
 * @author warrior
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ClientServiceImpl extends SuperServiceImpl<ClientMapper, Client> implements IClientService {
    private final static String LOCK_KEY_CLIENTID = "clientId:";

    private final PasswordEncoder passwordEncoder;

    private final DistributedLock lock;

    @Override
    public void saveClient(Client client) throws Exception {
        client.setClientSecret(passwordEncoder.encode(client.getClientSecretStr()));
        String clientId = client.getClientId();
        if (client.getId() == null) {
            client.setCreatorId(LoginUserContextHolder.getUser().getId());
        }
        super.saveOrUpdateIdempotency(client, lock
                , LOCK_KEY_CLIENTID+clientId
                , new QueryWrapper<Client>().eq("client_id", clientId)
                , clientId + "已存在");
    }

    @Override
    public PageResult<Client> listClient(Map<String, Object> params, boolean isPage) {
        Page<Client> page;
        if (isPage) {
            page = new Page<>(MapUtils.getInteger(params, "page"), MapUtils.getInteger(params, "limit"));
        } else {
            page = new Page<>(1, -1);
        }
        List<Client> list = baseMapper.findList(page, params);
        page.setRecords(list);
        return PageResult.<Client>builder().datas(list).code(200).count(page.getTotal()).build();
    }

    @Override
    public void delClient(long id) {
        baseMapper.deleteById(id);
    }

    @Override
    public Client loadClientByClientId(String clientId) {
        QueryWrapper<Client> wrapper = Wrappers.query();
        wrapper.eq("client_id", clientId);
        return this.getOne(wrapper);
    }
}
