package com.warrior.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.warrior.common.model.SysRole;
import com.warrior.mysql.mapper.SuperMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 角色数据库操作
 *
 * @author warrior
 */
@Mapper
public interface SysRoleMapper extends SuperMapper<SysRole> {
    List<SysRole> findList(Page<SysRole> page, @Param("r") Map<String, Object> params);

    List<SysRole> findAll();
}
