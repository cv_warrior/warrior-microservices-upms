package com.warrior.user.service;


import com.warrior.common.model.SysMenu;
import com.warrior.common.service.ISuperService;
import com.warrior.user.model.SysRoleMenu;

import java.util.List;
import java.util.Set;

/**
 * 角色菜单操作服务接口声明
 *
 * @author warrior
 */
public interface ISysRoleMenuService extends ISuperService<SysRoleMenu> {
    int save(Long roleId, Long menuId);

    int delete(Long roleId, Long menuId);

    List<SysMenu> findMenusByRoleIds(Set<Long> roleIds, Integer type);

    List<SysMenu> findMenusByRoleCodes(Set<String> roleCodes, Integer type);
}
