package com.warrior.gateway.config;

import com.warrior.common.config.DefaultAsyncTaskConfig;
import org.springframework.context.annotation.Configuration;

/**
 * 线程池配置、启用异步配置类
 *
 * @author warrior
 */
@Configuration
public class AsyncTaskExecutorConfig extends DefaultAsyncTaskConfig {
}
