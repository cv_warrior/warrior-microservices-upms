package com.warrior.uaa.service.impl;

import com.warrior.common.constant.SecurityConstants;
import com.warrior.common.feign.UserService;
import com.warrior.common.model.SysUser;
import com.warrior.common.utils.LoginUserUtils;
import com.warrior.uaa.service.WarriorUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 用户操作服务接口实现
 * @author warrior
 */
@Slf4j
@Service
public class UserDetailServiceImpl implements WarriorUserDetailsService {
    private static final String ACCOUNT_TYPE = SecurityConstants.DEF_ACCOUNT_TYPE;

    @Resource
    private UserService userService;

    @Override
    public boolean supports(String accountType) {
        return ACCOUNT_TYPE.equals(accountType);
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        SysUser sysUser = userService.findByUsername(username);
        if (sysUser == null) {
            throw new InternalAuthenticationServiceException("用户名或密码错误");
        }
        checkUser(sysUser);
        return LoginUserUtils.getLoginAppUser(sysUser);
    }

    @Override
    public UserDetails loadUserByUserId(String openId) {
        SysUser sysUser = userService.findByOpenId(openId);
        checkUser(sysUser);
        return LoginUserUtils.getLoginAppUser(sysUser);
    }

    @Override
    public UserDetails loadUserByMobile(String mobile) {
        SysUser sysUser = userService.findByMobile(mobile);
        checkUser(sysUser);
        return LoginUserUtils.getLoginAppUser(sysUser);
    }

    private void checkUser(SysUser sysUser) {
        if (sysUser != null && !sysUser.getEnabled()) {
            throw new DisabledException("用户已作废");
        }
    }
}
