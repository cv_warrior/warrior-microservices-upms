package com.warrior.user.service;


import com.warrior.common.model.SysRole;
import com.warrior.common.service.ISuperService;
import com.warrior.user.model.SysRoleUser;

import java.util.List;

/**
 * 用户角色操作服务接口声明
 *
 * @author warrior
 */
public interface ISysRoleUserService extends ISuperService<SysRoleUser> {
    int deleteUserRole(Long userId, Long roleId);

    int saveUserRoles(Long userId, Long roleId);

    /**
     * 根据用户id获取角色
     *
     * @param userId
     * @return
     */
    List<SysRole> findRolesByUserId(Long userId);

    /**
     * 根据用户ids 获取
     *
     * @param userIds
     * @return
     */
    List<SysRole> findRolesByUserIds(List<Long> userIds);
}
