package com.warrior.uaa.support.mobile;

import com.warrior.oauth2.common.token.BaseAuthenticationToken;
import com.warrior.oauth2.common.token.MobileAuthenticationToken;
import com.warrior.uaa.support.base.BaseAuthenticationConverter;
import com.warrior.uaa.utils.OAuthEndpointUtils;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.util.MultiValueMap;

import java.util.List;

/**
 * @Author warrior
 **/
public class MobileAuthenticationConverter extends BaseAuthenticationConverter {
    private final static String PARAM_MOBILE= "mobile";

    @Override
    protected String supportGrantType() {
        return MobileAuthenticationToken.GRANT_TYPE.getValue();
    }

    @Override
    protected List<String> paramNames() {
        return List.of(PARAM_MOBILE, OAuth2ParameterNames.PASSWORD);
    }

    @Override
    protected BaseAuthenticationToken getToken(MultiValueMap<String, String> parameters) {
        String mobile = OAuthEndpointUtils.getParam(parameters, PARAM_MOBILE);
        String password = OAuthEndpointUtils.getParam(parameters, OAuth2ParameterNames.PASSWORD);
        return new MobileAuthenticationToken(mobile, password);
    }
}
