package com.warrior.uaa.service;


import com.warrior.common.model.PageResult;
import com.warrior.uaa.model.TokenVo;

import java.util.Map;

/**
 * token 操作服务接口声明
 * @author warrior
 */
public interface ITokensService {
    /**
     * 查询token列表
     * @param params 请求参数
     * @param clientId 应用id
     */
    PageResult<TokenVo> listTokens(Map<String, Object> params, String clientId);
}
