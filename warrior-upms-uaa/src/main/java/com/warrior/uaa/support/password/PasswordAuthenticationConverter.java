package com.warrior.uaa.support.password;

import com.warrior.oauth2.common.token.BaseAuthenticationToken;
import com.warrior.oauth2.common.token.PasswordAuthenticationToken;
import com.warrior.uaa.support.base.BaseAuthenticationConverter;
import com.warrior.uaa.utils.OAuthEndpointUtils;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.util.MultiValueMap;

import java.util.List;

/**
 * @Author warrior
 **/
public class PasswordAuthenticationConverter extends BaseAuthenticationConverter {
    @Override
    protected String supportGrantType() {
        return PasswordAuthenticationToken.GRANT_TYPE.getValue();
    }

    @Override
    protected List<String> paramNames() {
        return List.of(OAuth2ParameterNames.USERNAME, OAuth2ParameterNames.PASSWORD);
    }

    @Override
    protected BaseAuthenticationToken getToken(MultiValueMap<String, String> parameters) {
        String username = OAuthEndpointUtils.getParam(parameters, OAuth2ParameterNames.USERNAME);
        String password = OAuthEndpointUtils.getParam(parameters, OAuth2ParameterNames.PASSWORD);
        return new PasswordAuthenticationToken(username, password);
    }
}
