package com.warrior.gateway.config;

import com.warrior.oauth2.common.config.DefaultWebFluxResourceServerConfig;
import org.springframework.context.annotation.Configuration;

/**
 * 资源服务器配置类
 * @author warrior
 */
@Configuration
public class ResourceServerConfig extends DefaultWebFluxResourceServerConfig {

}
