package com.warrior.uaa.support.openid;

import com.warrior.oauth2.common.token.BaseAuthenticationToken;
import com.warrior.oauth2.common.token.OpenIdAuthenticationToken;
import com.warrior.uaa.support.base.BaseAuthenticationConverter;
import com.warrior.uaa.utils.OAuthEndpointUtils;
import org.springframework.util.MultiValueMap;

import java.util.List;

/**
 * @Author warrior
 **/
public class OpenIdAuthenticationConverter extends BaseAuthenticationConverter {
    private final static String PARAM_OPENID = "openId";

    @Override
    protected String supportGrantType() {
        return OpenIdAuthenticationToken.GRANT_TYPE.getValue();
    }

    @Override
    protected List<String> paramNames() {
        return List.of(PARAM_OPENID);
    }

    @Override
    protected BaseAuthenticationToken getToken(MultiValueMap<String, String> parameters) {
        String openId = OAuthEndpointUtils.getParam(parameters, PARAM_OPENID);
        return new OpenIdAuthenticationToken(openId);
    }
}
