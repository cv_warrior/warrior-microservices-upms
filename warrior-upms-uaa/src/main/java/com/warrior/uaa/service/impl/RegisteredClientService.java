package com.warrior.uaa.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.warrior.common.constant.SecurityConstants;
import com.warrior.common.model.PageResult;
import com.warrior.oauth2.common.model.ClientDTO;
import com.warrior.uaa.model.Client;
import com.warrior.uaa.service.IClientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author warrior
 **/
@Slf4j
@RequiredArgsConstructor
@Service
public class RegisteredClientService {
    private final IClientService clientService;
    private final RedissonClient redisson;

    public void saveClient(Client client) throws Exception {
        clientService.saveClient(client);
        ClientDTO clientDto = BeanUtil.copyProperties(client, ClientDTO.class);
        redisson.getBucket(clientRedisKey(client.getClientId())).set(clientDto);
    }

    public PageResult<ClientDTO> listClient(Map<String, Object> params, boolean isPage) {
        PageResult<Client> clientPage = clientService.listClient(params, isPage);
        PageResult<ClientDTO> result = new PageResult<>();
        result.setCode(clientPage.getCode());
        result.setCount(clientPage.getCount());
        result.setDatas(BeanUtil.copyToList(clientPage.getDatas(), ClientDTO.class));
        return result;
    }

    public ClientDTO getById(long id) {
        return BeanUtil.copyProperties(clientService.getById(id), ClientDTO.class);
    }

    public void delClient(long id) {
        Client client = clientService.getById(id);
        if (client != null) {
            clientService.delClient(id);
            redisson.getBucket(clientRedisKey(client.getClientId())).delete();
        }
    }

    public ClientDTO loadClientByClientId(String clientId) {
        RBucket<ClientDTO> clientBucket = redisson.getBucket(clientRedisKey(clientId));
        ClientDTO clientDTO = clientBucket.get();
        if (clientDTO != null) {
            return clientDTO;
        }
        Client clientObj = clientService.loadClientByClientId(clientId);
        clientDTO = BeanUtil.copyProperties(clientObj, ClientDTO.class);
        clientBucket.set(clientDTO);
        return clientDTO;
    }

    public void loadAllClientToCache() {
        List<Client> clientList = clientService.list();
        clientList.forEach(c -> {
            ClientDTO clientDto = BeanUtil.copyProperties(c, ClientDTO.class);;
            redisson.getBucket(clientRedisKey(c.getClientId())).set(clientDto);
        });
    }

    public List<ClientDTO> list() {
        return BeanUtil.copyToList(clientService.list(), ClientDTO.class);
    }

    public ClientDTO getRegisteredClientByClientId(String clientId) {
        Client clientObj = clientService.loadClientByClientId(clientId);
        return BeanUtil.copyProperties(clientObj, ClientDTO.class);
    }

    private String clientRedisKey(String clientId) {
        return SecurityConstants.CACHE_CLIENT_KEY + ":" + clientId;
    }
}
