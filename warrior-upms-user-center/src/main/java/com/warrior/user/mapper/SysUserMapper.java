package com.warrior.user.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.warrior.common.model.SysUser;
import com.warrior.mysql.mapper.SuperMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 用户数据库操作
 *
 * @author warrior
 */
@Mapper
public interface SysUserMapper extends SuperMapper<SysUser> {
    /**
     * 分页查询用户列表
     *
     * @param page
     * @param params
     * @return
     */
    List<SysUser> findList(Page<SysUser> page, @Param("u") Map<String, Object> params);
}
