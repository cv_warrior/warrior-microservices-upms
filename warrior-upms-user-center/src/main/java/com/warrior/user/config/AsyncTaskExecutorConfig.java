package com.warrior.user.config;

import com.warrior.common.config.DefaultAsyncTaskConfig;
import org.springframework.context.annotation.Configuration;

/**
 * 线程池配置、启用异步
 * @Async quartz 需要使用
 * @author warrior
 */
@Configuration
public class AsyncTaskExecutorConfig extends DefaultAsyncTaskConfig {

}
