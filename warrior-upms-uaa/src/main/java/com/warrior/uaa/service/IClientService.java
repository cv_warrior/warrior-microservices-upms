package com.warrior.uaa.service;


import com.warrior.common.model.PageResult;
import com.warrior.common.service.ISuperService;
import com.warrior.uaa.model.Client;

import java.util.Map;

/**
 * 应用操作服务接口声明
 *
 * @author warrior
 */
public interface IClientService extends ISuperService<Client> {
    void saveClient(Client clientDto) throws Exception;

    /**
     * 查询应用列表
     * @param params
     * @param isPage 是否分页
     */
    PageResult<Client> listClient(Map<String, Object> params, boolean isPage);

    void delClient(long id);

    Client loadClientByClientId(String clientId);
}
