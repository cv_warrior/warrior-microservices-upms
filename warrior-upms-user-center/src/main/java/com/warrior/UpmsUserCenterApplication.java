package com.warrior;

import com.warrior.loadbalancer.annotation.EnableFeignInterceptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 用户中心服务启动类
 *
 * @author warrior
 */
@EnableDiscoveryClient
@EnableTransactionManagement
@EnableFeignInterceptor
@SpringBootApplication
@EnableFeignClients(basePackages = "com.warrior")
public class UpmsUserCenterApplication {
    public static void main(String[] args) {
        SpringApplication.run(UpmsUserCenterApplication.class, args);
    }
}
