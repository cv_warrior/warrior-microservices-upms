package com.warrior.uaa.controller;

import com.google.common.collect.Maps;
import com.warrior.common.model.PageResult;
import com.warrior.common.model.Result;
import com.warrior.uaa.model.Client;
import com.warrior.uaa.service.IClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 应用相关接口
 *
 * @author warrior
 */
@RestController
@RequestMapping("/clients")
public class ClientController {
    @Autowired
    private IClientService clientService;

    /**
     * 应用列表
     *
     * @param params
     * @return
     */
    @GetMapping("/list")
    public PageResult<Client> list(@RequestParam Map<String, Object> params) {
        return clientService.listClient(params, true);
    }

    /**
     * 根据id获取应用
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Client get(@PathVariable Long id) {
        return clientService.getById(id);
    }

    /**
     * 所有应用
     *
     * @return
     */
    @GetMapping("/all")
    public Result<List<Client>> allClient() {
        PageResult<Client> page = clientService.listClient(Maps.newHashMap(), false);
        return Result.succeed(page.getDatas());
    }

    /**
     * 删除应用
     *
     * @param id
     */
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        clientService.delClient(id);
    }

    /**
     * 保存或者修改应用
     *
     * @param client
     * @return
     * @throws Exception
     */
    @PostMapping("/saveOrUpdate")
    public Result<String> saveOrUpdate(@RequestBody Client client) throws Exception {
        clientService.saveClient(client);
        return Result.succeed("操作成功");
    }
}
