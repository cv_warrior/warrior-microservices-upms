package com.warrior.user.config;

import com.warrior.common.config.DefaultPasswordConfig;
import org.springframework.context.annotation.Configuration;

/**
 * 密码加密配置类
 *
 * @author warrior
 */
@Configuration
public class PasswordConfig extends DefaultPasswordConfig {
}
