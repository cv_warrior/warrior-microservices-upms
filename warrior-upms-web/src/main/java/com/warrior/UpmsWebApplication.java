package com.warrior;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 用户角色权限管理系统web启动类
 *
 * @author warrior
 */
@SpringBootApplication
public class UpmsWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(UpmsWebApplication.class, args);
    }
}
