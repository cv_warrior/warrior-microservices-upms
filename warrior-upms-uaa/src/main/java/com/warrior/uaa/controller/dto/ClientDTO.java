package com.warrior.uaa.controller.dto;

import com.warrior.uaa.model.Client;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

/**
 * 应用传输对象
 *
 * @author warrior
 */
@Setter
@Getter
public class ClientDTO extends Client {
    private static final long serialVersionUID = 1475637288060027265L;

    private List<Long> permissionIds;

    private Set<Long> serviceIds;
}
