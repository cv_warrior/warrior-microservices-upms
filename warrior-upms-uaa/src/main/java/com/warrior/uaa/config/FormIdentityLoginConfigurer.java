package com.warrior.uaa.config;

import com.warrior.common.constant.SecurityConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * @Author warrior
 **/
@RequiredArgsConstructor
public class FormIdentityLoginConfigurer extends AbstractHttpConfigurer<FormIdentityLoginConfigurer, HttpSecurity> {
    private final LogoutSuccessHandler logoutSuccessHandler;
    private final LogoutHandler logoutHandler;

    @Override
    public void init(HttpSecurity http) throws Exception {
        http.formLogin(formLogin -> formLogin
                        .loginPage(SecurityConstants.LOGIN_PAGE)
                        .loginProcessingUrl(SecurityConstants.OAUTH_LOGIN_PRO_URL)
                        .successHandler(new SavedRequestAwareAuthenticationSuccessHandler()))
                .logout(logout -> logout
                        .logoutUrl(SecurityConstants.LOGOUT_URL)
                        .logoutSuccessHandler(logoutSuccessHandler)
                        .addLogoutHandler(logoutHandler)
                        .deleteCookies("JSESSIONID")
                        .clearAuthentication(true)
                        .invalidateHttpSession(true))
                .headers(header -> header
                        // 避免iframe同源无法登录许iframe
                        .frameOptions(HeadersConfigurer.FrameOptionsConfig::sameOrigin))
                .csrf(AbstractHttpConfigurer::disable)
                .sessionManagement(session -> session
                        .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED));
    }
}

