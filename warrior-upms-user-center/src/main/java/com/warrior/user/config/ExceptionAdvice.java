package com.warrior.user.config;

import com.warrior.common.exception.DefaultExceptionAdvice;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * 统一异常处理
 *
 * @author warrior
 */
@ControllerAdvice
public class ExceptionAdvice extends DefaultExceptionAdvice {
}
