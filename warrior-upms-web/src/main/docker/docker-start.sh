#!/bin/bash
APP_ENV=${appenv:-dev}
APP_NAME=${appname}
APP_VERSION=${appversion:-1.0-SNAPSHOT}
LOG_MAX_SIZE=${log_max_size:-20MB}

# change directory to program dir
FWDIR="$(cd `dirname $0`; pwd)"
cd ${FWDIR}

if [ ! -d ${FWDIR}/java.pid ]; then
    touch ${FWDIR}/java.pid
fi

OSUSER=$(id -nu)
PSNUM=$(cat ${FWDIR}/java.pid)
if [[ "$PSNUM" -ne "" ]]; then
    echo ${APP_NAME}" has been started! stop first."
    exit;
fi

echo "Running in docker!"

java  -XX:+UnlockExperimentalVMOptions \
      -XX:MaxRAMPercentage=90.0  \
      -XX:+HeapDumpOnOutOfMemoryError \
      -XX:HeapDumpPath=/app/${APP_NAME}/log/ \
      -Dspring.profiles.active=${APP_ENV} \
      -Dapp.name=${APP_NAME} \
      -Dapp.env=${APP_ENV} \
      -Dmax.size=${LOG_MAX_SIZE} \
      -Duser.dir=${FWDIR} \
      -Dfile.encoding=UTF-8 \
      -Dsun.jnu.encoding=UTF-8 \
      -jar ${APP_NAME}.jar

