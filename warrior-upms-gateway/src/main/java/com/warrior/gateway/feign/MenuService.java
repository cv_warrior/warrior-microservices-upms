package com.warrior.gateway.feign;

import com.warrior.common.constant.ServiceNameConstants;
import com.warrior.common.model.SysMenu;
import com.warrior.gateway.feign.fallback.MenuServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * 菜单feign 接口服务`
 *
 * @author warrior
 */
@FeignClient(name = ServiceNameConstants.USER_SERVICE, fallbackFactory = MenuServiceFallbackFactory.class, dismiss404 = true)
public interface MenuService {
    /**
     * 角色菜单列表
     *
     * @param roleCodes
     */
    @GetMapping(value = "/menus/{roleCodes}")
    List<SysMenu> findByRoleCodes(@PathVariable("roleCodes") String roleCodes);
}
