package com.warrior;

import com.warrior.loadbalancer.annotation.EnableBaseFeignInterceptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 用户角色权限管理网关启动类
 *
 * @author warrior
 */
@EnableFeignClients
@EnableBaseFeignInterceptor
@EnableDiscoveryClient
@SpringBootApplication
public class UpmsGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(UpmsGatewayApplication.class, args);
    }
}
